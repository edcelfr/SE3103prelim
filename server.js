const http = require('http');
const ejs = require('ejs');
const Client = require('mariasql');
const querystring = require('querystring');

const server = http.createServer((request, response) => {

    const url = request.url;

    const client = new Client({
    host: 'localhost',
    user: 'root',
    password: '',
    db: 'dbcomplaints'
});

    if (url === '/') {

            client.query('SELECT * FROM complaints', (error, results) => {

                response.writeHead(200, {'Content-Type' : 'text\html'});
                ejs.renderFile('dumb-communications-inc.ejs', {results}, (ejsError, html) => {
                    response.end(html);
                });

            });       

    } else if (url === '/complain'){

        ejs.renderFile('customer-complaints.ejs', (ejsError, html) => {
            response.end(html);
        });

    } else if (url === '/submit' && request.method === 'POST') {

        request.on('data', (chunk) => {

            let data = chunk.toString();
            let parsedData = querystring.parse(data);

            client.query(`INSERT INTO complaints VALUES (
            ${parsedData.id}, 
            '${parsedData.area}', 
            '${parsedData.complaint}', 
            '${parsedData.date}')`, (error, results) => {

                        if(error) {
                            console.log("error", error.message);
                        }

                    });

        });

        request.on('end', () => {        

            client.query('SELECT * FROM complaints', (error, results) => {

                if(error) {
                    console.log("error happened");
                }

                ejs.renderFile('dumb-communications-inc.ejs', {results}, {}, (ejsError, html) => {
                    response.end(html);
                });

            });

        });
        
    };

});

server.listen(8084, () => {
    console.log('server is now listening...')
});